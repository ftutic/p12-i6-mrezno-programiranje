﻿using System.Net.Sockets;
using System.Text;

namespace SocketKlijentGUI
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                TcpClient klijent = new TcpClient(tbServerIP.Text, Int32.Parse(tbServerPort.Text));
                Byte[] poruka = Encoding.ASCII.GetBytes(tbMessage.Text);
                NetworkStream stream = klijent.GetStream();
                stream.Write(poruka, 0, poruka.Length);
                stream.Close();
                klijent.Close();
                lblReturnMsg.Text = "Poruka je uspješno poslana";
            }
            catch (Exception ex)
            {

                if (ex is SocketException)
                {
                    lblReturnMsg.Text = "Nije moguće spojiti se na server";
                }
            }
        }

    }
}