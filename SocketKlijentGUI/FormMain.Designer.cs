﻿namespace SocketKlijentGUI
{
    partial class FormMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tbServerIP = new TextBox();
            lblServerIP = new Label();
            lblServerPort = new Label();
            tbServerPort = new TextBox();
            lblMessage = new Label();
            tbMessage = new TextBox();
            btnSend = new Button();
            lblReturnMsg = new Label();
            SuspendLayout();
            // 
            // tbServerIP
            // 
            tbServerIP.Location = new Point(137, 15);
            tbServerIP.Name = "tbServerIP";
            tbServerIP.Size = new Size(338, 23);
            tbServerIP.TabIndex = 0;
            // 
            // lblServerIP
            // 
            lblServerIP.AutoSize = true;
            lblServerIP.Location = new Point(12, 18);
            lblServerIP.Name = "lblServerIP";
            lblServerIP.Size = new Size(55, 15);
            lblServerIP.TabIndex = 1;
            lblServerIP.Text = "Server IP:";
            // 
            // lblServerPort
            // 
            lblServerPort.AutoSize = true;
            lblServerPort.Location = new Point(12, 57);
            lblServerPort.Name = "lblServerPort";
            lblServerPort.Size = new Size(67, 15);
            lblServerPort.TabIndex = 3;
            lblServerPort.Text = "Server Port:";
            // 
            // tbServerPort
            // 
            tbServerPort.Location = new Point(137, 54);
            tbServerPort.Name = "tbServerPort";
            tbServerPort.Size = new Size(338, 23);
            tbServerPort.TabIndex = 2;
            // 
            // lblMessage
            // 
            lblMessage.AutoSize = true;
            lblMessage.Location = new Point(12, 99);
            lblMessage.Name = "lblMessage";
            lblMessage.Size = new Size(47, 15);
            lblMessage.TabIndex = 5;
            lblMessage.Text = "Poruka:";
            // 
            // tbMessage
            // 
            tbMessage.Location = new Point(137, 96);
            tbMessage.Name = "tbMessage";
            tbMessage.Size = new Size(338, 23);
            tbMessage.TabIndex = 4;
            // 
            // btnSend
            // 
            btnSend.Location = new Point(12, 146);
            btnSend.Name = "btnSend";
            btnSend.Size = new Size(463, 53);
            btnSend.TabIndex = 6;
            btnSend.Text = "Pošalji!";
            btnSend.UseVisualStyleBackColor = true;
            btnSend.Click += btnSend_Click;
            // 
            // lblReturnMsg
            // 
            lblReturnMsg.AutoSize = true;
            lblReturnMsg.Location = new Point(248, 128);
            lblReturnMsg.Name = "lblReturnMsg";
            lblReturnMsg.Size = new Size(0, 15);
            lblReturnMsg.TabIndex = 7;
            // 
            // FormMain
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(497, 213);
            Controls.Add(lblReturnMsg);
            Controls.Add(btnSend);
            Controls.Add(lblMessage);
            Controls.Add(tbMessage);
            Controls.Add(lblServerPort);
            Controls.Add(tbServerPort);
            Controls.Add(lblServerIP);
            Controls.Add(tbServerIP);
            Name = "FormMain";
            Text = "Socket Client";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TextBox tbServerIP;
        private Label lblServerIP;
        private Label lblServerPort;
        private TextBox tbServerPort;
        private Label lblMessage;
        private TextBox tbMessage;
        private Button btnSend;
        private Label lblReturnMsg;
    }
}