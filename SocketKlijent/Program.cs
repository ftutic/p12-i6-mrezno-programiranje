﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketKlijent {
    class Program {
        static void Main(string[] args) {
            while (true) {
                Console.Write("Upišite poruku: ");
                string porukaString = Console.ReadLine();
                TcpClient klijent = new TcpClient("127.0.0.1", 50000);
                Byte[] poruka = Encoding.ASCII.GetBytes(porukaString);
                NetworkStream stream = klijent.GetStream();
                Console.WriteLine(DateTime.Now + " - šaljem poruku!");
                stream.Write(poruka, 0, poruka.Length);
                stream.Close();
                Console.WriteLine(DateTime.Now + " - poruka poslana!");
                klijent.Close();
            }
        }
    }
}
